﻿using UnityEngine;

public static class RendererExtensions
{
	//NOTE: Lets you know if in frustrum but but may be obscured by other objects
	public static bool IsVisibleFrom(Renderer renderer, Camera camera)
	{
		Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
		return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
	}
}
