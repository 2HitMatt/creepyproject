﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour, IInteractable
{
    bool collected = false;
    public void Interact(GameObject g)
    {
        if(collected == false)
        {
            collected = true;
            CollectibleCounter.collectibleCounter.Collected++;
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
