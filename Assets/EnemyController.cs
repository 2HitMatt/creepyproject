﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnemyState{
    idle, chasing, wandering
}

public class EnemyController : MonoBehaviour
{
    public EnemyState state = EnemyState.idle;
    public float movementSpeed = 3f;
    public LayerMask sightLayer;
    public Rigidbody rb;

    [SerializeField] bool canSeePlayer = false;
    [SerializeField] float stateTimer = 0;
    [SerializeField] Vector3 wanderDirection = new Vector3();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float rayDist = 100;

        //can I see the player?
        canSeePlayer = false; //reset then check
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDist, sightLayer))
        {
            if(hit.transform.tag=="Player")
            {
                canSeePlayer = true;
            }
            rayDist = hit.distance;
        }
        Debug.DrawRay(transform.position, transform.forward * rayDist, Color.green);
        //always chase player if they can see them :D
        if (canSeePlayer)
        {
            state = EnemyState.chasing;
        }
        //states
        switch (state)
        {
            case EnemyState.idle:
                stateTimer -= Time.deltaTime;
                rb.velocity = new Vector3(0, 0, 0);
                if (stateTimer <= 0)
                {
                    stateTimer = 1;
                    state = EnemyState.wandering;
                    RandomiseDirection();
                }
                break;
            case EnemyState.chasing:
                if(canSeePlayer)
                {
                    rb.velocity = transform.forward * movementSpeed ;
                }
                else
                {
                    //lost sight, change state
                    stateTimer = 1;
                    state = EnemyState.wandering;
                    RandomiseDirection();
                }
                break;
            case EnemyState.wandering:
                stateTimer -= Time.deltaTime;
                rb.velocity = wanderDirection * movementSpeed;
                if (stateTimer <= 0)
                {
                    stateTimer = 1;
                    state = EnemyState.idle;
                }
                break;
            default:
                break;
        }

    }

    void RandomiseDirection()
    {
        wanderDirection = (new Vector3(Random.Range(-1.0f, 1.0f), 0.0f, Random.Range(-1.0f, 1.0f))).normalized;
    }
}
