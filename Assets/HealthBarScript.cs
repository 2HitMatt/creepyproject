﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
    float max = 10, hp = 10;
    public Slider slider;
    public Image image;

    public void SetMaxHealth(int max)
    {
        //slider.maxValue = max;
        this.max = max;
        updateBar();
    }

    public void SetHealth(int hp)
    {
        //slider.value = hp;
        this.hp = hp;
        updateBar();
    }

    void updateBar()
    {
        image.fillAmount = hp / max;
    }
}
