﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ButtonsInput : MonoBehaviour
{
    public Button start, quit;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        if (start != null)
        {
            start.onClick.AddListener(StartGame);
        }
        if (quit != null)
        {
            quit.onClick.AddListener(QuitName);
        }
    }

    void StartGame()
    {

        //START THE GAME
        SceneManager.LoadScene("SampleScene");
    }
    void QuitName()
    {
        Application.Quit();
    }
}
