﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportDoll : MonoBehaviour
{
    public Camera camera;
    public Renderer renderer;
    bool seenByCamera = false;
    int currentNodeIndex = 0;
    public List<Transform> telePortPositions = new List<Transform>();
    
    public LayerMask sightLayer;

    // Start is called before the first frame update
    void Start()
    {
        renderer = gameObject.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //dont need to see me after you've collected 4 heirlooms
        if(CollectibleCounter.collectibleCounter.Collected >= CollectibleCounter.collectibleCounter.CollectedMax-1)
        {
            Destroy(gameObject);
            return;
        }

        if (camera != null)
        {

            bool playerCanSeeMe = PlayerCanSeeMe();
            //we were visible to the player, now its time to hide elsewhere
            if(seenByCamera && !playerCanSeeMe)
            {
                Teleport();
            }
            seenByCamera = playerCanSeeMe;
        }
        else
        {
            camera = FindObjectOfType<Camera>();
            
        }
    }

    bool PlayerCanSeeMe()
    {
        float rayDist = 100;
        bool onScreen = RendererExtensions.IsVisibleFrom(renderer, camera);
        //Debug.Log("Seen = " + onScreen);

        //onScreen only checks frustrum, not whether it can be viewed, so we'll use a raycast as well
        bool canSeePlayer = false; //reset then check
        RaycastHit hit;

        if (Physics.Raycast(transform.position, transform.forward, out hit, rayDist, sightLayer))
        {
            if (hit.transform.tag == "Player")
            {
                canSeePlayer = true;
            }
            rayDist = hit.distance;
        }
        Debug.DrawRay(transform.position, transform.forward * rayDist, Color.green);
        Debug.Log("Lilly can see: " + (onScreen && canSeePlayer));
        return onScreen && canSeePlayer;
    }

    void Teleport()
    {
        int index = currentNodeIndex;
        int attempts = 0;
        while(index == currentNodeIndex)
        {
            index = Random.Range(0, telePortPositions.Count);
            attempts++;
            if (attempts > 10)
                break; //fuck it
        }
        transform.position = telePortPositions[index].position;
        currentNodeIndex = index;
    }
}
