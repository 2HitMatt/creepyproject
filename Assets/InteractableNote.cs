﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableNote : MonoBehaviour, IInteractable
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    public void Interact(GameObject g)
    {
        SpookyNote.DisplaySpookyNote();

        Gun gun = g.GetComponent<Gun>();
        if (gun != null)
            gun.note = this;
    }
}
