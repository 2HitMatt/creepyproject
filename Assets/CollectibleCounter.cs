﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CollectibleCounter : MonoBehaviour
{
    public static CollectibleCounter collectibleCounter;

    private int collected = 0;

    public int Collected
    {
        get { return collected; }
        set { 
            collected = value;
            updateText();
        }
    }
    private int collectedMax = 5;

    public int CollectedMax
    {
        get { return collectedMax; }
        set { 
            collectedMax = value;
            updateText();
        }
    }

    public TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        collectibleCounter = this;
        updateText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void updateText()
    {
        text.text = collected + "/" + collectedMax;
    }
}
