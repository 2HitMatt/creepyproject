﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{

    public float damage = 10;
    public float range = 100;
    public float impactForce = 30;
    public float fireRate = 0.6f;
    public int bullets = 15;
    public int bulletsMax = 15;
    public bool automatic = true;

    public Camera camera;

    public ParticleSystem muzzleFlash;
    public GameObject impactEffect;

    public float nextTimeToFire = 0f;
    public LayerMask playerLayer;
    public LayerMask interactLayer;

    //most recent interaction
    public InteractableNote note = null;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if talked to a note and walked away, hide note
        if(note != null && Vector3.Distance(transform.position, note.transform.position) > 2)
        {
            note = null;
            SpookyNote.HideSpookyNote();
        }


        //TIMERS
        if(nextTimeToFire > 0)
            nextTimeToFire -= Time.deltaTime;
        
        //INPUT
        //SHOOT
        //if (automatic)
        //{
        //    if (Input.GetButton("Fire1") && nextTimeToFire <= 0)
        //    {
        //        bullets--;
        //        Shoot();
        //        if(bullets > 0)
        //            nextTimeToFire = fireRate;
        //        else
        //        {
        //            nextTimeToFire = 1;
        //            bullets = bulletsMax;
        //        }
        //    }
        //}
        //else
        //{ 
        //    if(Input.GetButtonDown("Fire1"))
        //    {
        //        Shoot();
        //    }
            
        //}

        //USE
        if (Input.GetButtonDown("Use"))
        {
            Interact();
        }



    }

    void Shoot()
    {
        muzzleFlash.Play();
        RaycastHit hit;
        if(Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, range, ~playerLayer))
        {
            Debug.Log(hit.transform.name);

            DamageTaker dt = hit.transform.GetComponent<DamageTaker>();
            
            if(dt != null)
            {
                dt.TakeDamage(damage);
            }

            if(hit.rigidbody != null)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }
            GameObject effect= Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(effect, 1);
        }
    }

    void Interact()
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.transform.position, camera.transform.forward, out hit, 2, ~playerLayer))
        {
            Debug.Log(hit.transform.name);
            //IS IT A NOTE?!
            IInteractable interactable = hit.transform.GetComponent<IInteractable>();

            if(interactable != null)
            {
                interactable.Interact(gameObject);
            }

            //GameObject effect = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            //Destroy(effect, 1);
        }
        else
        {
            //MISS

            //but if holding note, hide it if we're not interacting
            if (note != null)
            {
                note = null;
                SpookyNote.HideSpookyNote();
            }
        }
    }
}
