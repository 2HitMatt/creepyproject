﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpOut : MonoBehaviour
{
    public bool jumpOut = false;
    public float jumpTimeMax = 1;
    public float jumpTime = 0;
    public AnimationCurve animationCurve;
    public bool loop = false; //for testing
    public bool dieEndOfLoop = true;
    public Transform targetPosition;
    Vector3 startPosition;
    Quaternion startRotation;
    public GameObject scaryNoise;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
        startRotation = transform.rotation;
    }

    public void Jump()
    {
        Instantiate(scaryNoise, transform.position, transform.rotation);
        jumpOut = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (jumpOut)
        {
            //update jumpTime
            jumpTime += Time.deltaTime;
            if(jumpTime > jumpTimeMax)
            {
                if (loop)
                    jumpTime = 0;
                else
                {
                    jumpTime = jumpTimeMax;
                    if (dieEndOfLoop)
                    {
                        Destroy(gameObject);
                        return; //dont do the rest of this update
                    }
                }
            }

            float jumpRatio = jumpTime / jumpTimeMax;//0-1
            float curveValue = animationCurve.Evaluate(jumpRatio);

            //Debug.Log("JumpRatio: " + jumpRatio + " CurveValue: " + curveValue);


            transform.position = Vector3.Lerp(startPosition, targetPosition.position, curveValue);
            transform.rotation = Quaternion.Lerp(startRotation, targetPosition.rotation, curveValue);



        }
    }
}
