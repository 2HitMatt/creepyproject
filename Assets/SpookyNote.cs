﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpookyNote : MonoBehaviour
{
    public static SpookyNote SPOOKY_NOTE;
    public Image note;

    // Start is called before the first frame update
    void Start()
    {
        SPOOKY_NOTE = this;

        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void DisplaySpookyNote()
    {
        if(SPOOKY_NOTE != null)
            SPOOKY_NOTE.gameObject.SetActive(true);

        //TODO swap image out as well ;)
    }
    public static void HideSpookyNote()
    {
        if (SPOOKY_NOTE != null)
            SPOOKY_NOTE.gameObject.SetActive(false);
    }
}
