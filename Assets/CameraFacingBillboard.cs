﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFacingBillboard : MonoBehaviour
{

    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    

    //Orient the camera after all movement is completed this frame to avoid jittering
    void LateUpdate()
    {
        if (camera != null)
        {
            Vector3 targetPosition = new Vector3(camera.transform.position.x, transform.position.y, camera.transform.position.z);
            transform.LookAt(targetPosition,
                Vector3.up);

           // transform.LookAt(transform.position + camera.transform.rotation * Vector3.forward,
           //     camera.transform.rotation * Vector3.up);
        }
        else
        {
            camera = FindObjectOfType<Camera>();
        }
    }
}
