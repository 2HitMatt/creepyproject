﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{

    public int hp = 100;
    public int hpMax = 100;
    public HealthBarScript healthBar;

    int painLayer;

    float invincibleTimer = 0;
    float invincibleTimerMax = 0.5f;

    // Start is called before the first frame update
    void Start()
    {
        painLayer = LayerMask.NameToLayer("Pain");
        healthBar.SetMaxHealth(hpMax);
        healthBar.SetHealth(hp);
    }

    // Update is called once per frame
    void Update()
    {
        if (invincibleTimer > 0)
            invincibleTimer -= Time.deltaTime;
        if(hp <= 0)
        {
            SceneManager.LoadScene("Dead");
        }
    }

    public void TakeDamage(int dmg)
    {
        if (invincibleTimer <= 0)
        {
            invincibleTimer = invincibleTimerMax;
            hp -= dmg;
            if (hp < 0)
                hp = 0;

            healthBar.SetHealth(hp);
        }
        
    }


}
