﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum DoorType
{
    openenable, changeScenes
}

public class Door : MonoBehaviour, IInteractable
{
    public DoorType doorType = DoorType.openenable;
    public bool locked = false;
    public string newScene;
    public void Interact(GameObject g)
    {
        if (!locked)
        {
            if (doorType == DoorType.changeScenes)
            {
                SceneManager.LoadScene(newScene);
            }
            else
            {
                //too much effort to swing doors open, destroy them
                Destroy(gameObject);
                return;
            }
        }
        else
        {
            //locked, now what
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (locked)
        {
            if (doorType == DoorType.changeScenes)
            {
                if (CollectibleCounter.collectibleCounter.Collected >= CollectibleCounter.collectibleCounter.CollectedMax)
                    locked = false;
            }
            else
            {
                if (CollectibleCounter.collectibleCounter.Collected >= CollectibleCounter.collectibleCounter.CollectedMax-1)
                    locked = false;
            }
        }
    }
}
